$(document).ready(function() {
  $('.toc-wrapper').pushpin({
    offset: 0,
  });
  $('.scrollspy').scrollSpy({
    scrollOffset: 20,
  });

  $('.headline-wrapper').click(function() {
    $('.typed-text').typed({
      strings: ['Web Developer?', 'Web Designer?', 'Hi I am Fabian Kresler!'],
      typeSpeed: 10,
      startDelay: 200,
      backSpeed: 10,
      backDelay: 1000,
      loop: false,
      showCursor: false,
    });
  });

  $("#project-lctmt").iziModal({
    title: 'LOEWE Central Test Management Tools',
    subtitle: '2016-2017',
    headerColor: '#002236',
    fullscreen: false,
    openFullscreen: true,
    closeOnEscape: true,
    transitionIn: 'bounceInDown',
    transitionOut: 'bounceOutDown',
    transitionInOverlay: 'bounceInDown',
    transitionOutOverlay: 'bounceOutDown',
  });

  $('#project-sportstogether').iziModal({
    title: 'SportsTogether',
    subtitle: '2016',
    headerColor: '#002236',
    fullscreen: false,
    openFullscreen: true,
    closeOnEscape: true,
    transitionIn: 'bounceInDown',
    transitionOut: 'bounceOutDown',
    transitionInOverlay: 'bounceInDown',
    transitionOutOverlay: 'bounceOutDown',
  });

  $('#project-leaguemains').iziModal({
    title: 'LeagueMains',
    subtitle: '2015-2016',
    headerColor: '#002236',
    fullscreen: false,
    openFullscreen: true,
    closeOnEscape: true,
    transitionIn: 'bounceInDown',
    transitionOut: 'bounceOutDown',
    transitionInOverlay: 'bounceInDown',
    transitionOutOverlay: 'bounceOutDown',
  });

  $(window).on('hashchange', function(e){
    history.replaceState ("", document.title, e.originalEvent.oldURL);
  });
});
